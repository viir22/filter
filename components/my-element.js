import {LitElement, html,css} from 'lit-element';
import GetMyPostsDm from './get-my-posts-dm.js';

class MyElement extends LitElement {
  static get styles(){
    return css`
      #title {
        font-size: 30px;
        color: #030303;
      }
      ul {
        margin-left: 15px;
      }
      li {
        font-size: 24px;
        padding-bottom: 5px;
      }
      #button {
        background-color : '#F0EEEE';
        color: #030303;
        font-size: 24px;
        cursor: pointer;
        border: solid 2px #9B9897;
        border-radius: 8px;
        padding: 5px 12px;
      }
    `;
  }

  static get properties(){
    return{
      propObj :{type: Object},
      propStr : {type: String},
      users : {type: Array},
      propNum : {type: Number},

    }
  }
  constructor(){
   super();
   this.propObj = {};
   this.propStr = '';
   this.users = [];
   this.propNum = 0;

  }



  _getPosts(){
    let _dmGetPosts = new GetMyPostsDm();
    _dmGetPosts.addEventListener('success-call', this._setPosts.bind(this));
    _dmGetPosts.addEventListener('error-call', this._showModalError.bind(this));
    _dmGetPosts.generateRequest();
  }
  _showModalError(configError){
    this.errorModal = configError;
  }
  _setPosts(data){
    this.users = data.detail;
  }

  render() {
    return html`
      <div id="title">${this.propStr}</div>
      <label id="title">${this.propNum}</label>
      <ul>
        ${this.users.map(item =>
          html`<li>${item.name} ${item.age}</li>`
          )}
      </ul>
      <label>${this.propObj.name}</label>
      <button id="button" @click="${this._getPosts}">Click</button>
    `;
  }
}

customElements.define('my-element', MyElement);




